rem set JAVA_HOME="c:\Program Files\Java\jre1.8.0_144"
set JAVA_HOME="c:\Program Files\Java\jre1.8.0_151"
set JMETER_HOME=d:\jMeter-3.0
set JMX_HOME=d:\PerfTest

rem :exec_batch

rem ENV=test, dev, prod(www), test.beta
set ENV=%~1
set APPLICATION=%~2
set DOMAIN=%~3
set PRODUCT=%APPLICATION%.%DOMAIN%

set JENKINS_BUILD_NUMBER=%4
set SCRIPT_NAME=%~5
set TOTAL_USERS=%~6
set TOTAL_PERIOD=%~7
set SCENARIO_TYPE=%~8

set SCRIPT_FILE=%JMX_HOME%\Scripts\%SCRIPT_NAME%

set INPUT_JTL_FILE=%JMX_HOME%\Listeners\%ENV%.%PRODUCT%\Publisher_results.xml
set ERRORS_FILE=%JMX_HOME%\Listeners\%ENV%.%PRODUCT%\LoadTest_Errors.xml

del %INPUT_JTL_FILE%
del %ERRORS_FILE%
del %JMX_HOME%\Listeners\%ENV%.%PRODUCT%\*.csv

@call %JMETER_HOME%\bin\jmeter.bat -Jsample_variables=TTFB,BACKEND,FRONTEND,DOM,PROCESSING,RENDERING,ONLOAD,FULLLOAD -n -t %SCRIPT_FILE% -l %INPUT_JTL_FILE% -JpHost=%ENV%.%PRODUCT% -JpValidation=false -JpVUserTotal=%TOTAL_USERS% -JpLoadPeriod=%TOTAL_PERIOD% -JpScType=%SCENARIO_TYPE%
timeout 10

set CLASSPATH=%CLASSPATH%;%JMETER_HOME%\lib\xalan-2.7.2.jar;%JMETER_HOME%\lib\serializer-2.7.2.jar;%JMETER_HOME%\lib\xercesImpl-2.11.0.jar;%JMETER_HOME%\lib\xml-apis-1.4.01.jar;.\lib\jcommon-1.0.23.jar;.\lib\jfreechart-1.0.19.jar;.\lib\reporter.jar

set INPUT_TEMPLATE_PATH=.\_template
rem variables for width and height of charts in pixels.
set CHART_WIDTH=1200
set CHART_HEIGHT=600
rem variable for granulation in ms
set CHART_GRANULATION_IN_MS=30000

rem /*generate timestamp for new report folder*/
for /f %%a IN ('WMIC OS GET LocalDateTime ^| find "."') do set DTS=%%a
set DATE_TIME=%DTS:~0,8%-%DTS:~8,4%
set DATE_TIME_FMT=%DTS:~0,4%.%DTS:~4,2%.%DTS:~6,2% %DTS:~8,2%:%DTS:~10,2%
rem echo '%DATE_TIME_FMT%'
rem /*create folders and subfolders for the new report and copy template files from appropriate _template folder*/
rem set REPORTDIR=.\reports\%DATE_TIME%_%JENKINS_BUILD_NUMBER%_%ENV%.%PRODUCT%
rem set REPORTDIR_xml=./reports/%DATE_TIME%_%JENKINS_BUILD_NUMBER%_%ENV%.%PRODUCT%

set REPORTDIR=.\reports\%JENKINS_BUILD_NUMBER%_%ENV%.%PRODUCT%
set REPORTDIR_xml=./reports/%JENKINS_BUILD_NUMBER%_%ENV%.%PRODUCT%

mkdir %REPORTDIR%
mkdir %REPORTDIR%\static
mkdir %REPORTDIR%\static\images
mkdir %REPORTDIR%\static\css
mkdir %REPORTDIR%\static\js
mkdir %REPORTDIR%\details
mkdir %REPORTDIR%\charts
mkdir %REPORTDIR%\data

copy %INPUT_TEMPLATE_PATH%\static\images\* %REPORTDIR%\static\images
copy %INPUT_TEMPLATE_PATH%\static\css\* %REPORTDIR%\static\css
copy %INPUT_TEMPLATE_PATH%\static\js\* %REPORTDIR%\static\js

rem # build chart for Threads State Over Time and save it with %REPORTDIR%\charts\ThreadsStateOverTime.png name;
%JAVA_HOME%\bin\java.exe -jar %JMETER_HOME%\lib\ext\CMDRunner.jar --tool Reporter --generate-png %REPORTDIR%\charts\ThreadsStateOverTime.png --input-jtl %INPUT_JTL_FILE% --plugin-type ThreadsStateOverTime --width %CHART_WIDTH% --height %CHART_HEIGHT% --relative-times no --granulation %CHART_GRANULATION_IN_MS% --paint-gradient no
%JAVA_HOME%\bin\java.exe -jar %JMETER_HOME%\lib\ext\CMDRunner.jar --tool Reporter --generate-csv %REPORTDIR%\data\ThreadsStateOverTime.csv --input-jtl %INPUT_JTL_FILE% --plugin-type ThreadsStateOverTime --relative-times no --granulation %CHART_GRANULATION_IN_MS%
%JAVA_HOME%\bin\java.exe -cp .\lib\saxon9he.jar net.sf.saxon.Transform -o:%REPORTDIR%\data\ThreadsStateOverTime.xml -it:main .\_template\xsl\csv-to-xml_v2.xslt pathToCSV="file:%REPORTDIR_xml%/data/ThreadsStateOverTime.csv" 
%JAVA_HOME%\bin\java.exe org.apache.xalan.xslt.Process -IN %REPORTDIR%\data\ThreadsStateOverTime.xml -XSL %INPUT_TEMPLATE_PATH%\xsl\graphs.xsl -OUT %REPORTDIR%\details\report_thot.html -param dateReport "%DATE_TIME_FMT%" -param graphName "ThreadsStateOverTime" -param graphChart "../charts/ThreadsStateOverTime.png"

rem # build chart for Hits Per Second and save it with %REPORTDIR%\charts\HitsPerSecond.png name;
%JAVA_HOME%\bin\java.exe -jar %JMETER_HOME%\lib\ext\CMDRunner.jar --tool Reporter --generate-png %REPORTDIR%\charts\HitsPerSecond.png --input-jtl %INPUT_JTL_FILE% --plugin-type HitsPerSecond --width %CHART_WIDTH% --height %CHART_HEIGHT% --relative-times no --granulation %CHART_GRANULATION_IN_MS% --auto-scale yes --paint-gradient no
%JAVA_HOME%\bin\java.exe -jar %JMETER_HOME%\lib\ext\CMDRunner.jar --tool Reporter --generate-csv %REPORTDIR%\data\HitsPerSecond.csv --input-jtl %INPUT_JTL_FILE% --plugin-type HitsPerSecond --relative-times no --granulation %CHART_GRANULATION_IN_MS%
%JAVA_HOME%\bin\java.exe -cp .\lib\saxon9he.jar net.sf.saxon.Transform -o:%REPORTDIR%\data\HitsPerSecond.xml -it:main .\_template\xsl\csv-to-xml_v2.xslt pathToCSV="file:%REPORTDIR_xml%/data/HitsPerSecond.csv" 
%JAVA_HOME%\bin\java.exe org.apache.xalan.xslt.Process -IN %REPORTDIR%\data\HitsPerSecond.xml -XSL %INPUT_TEMPLATE_PATH%\xsl\graphs.xsl -OUT %REPORTDIR%\details\report_hps.html -param dateReport "%DATE_TIME_FMT%" -param graphName "HitsPerSecond" -param graphChart "../charts/HitsPerSecond.png"

rem # build chart for Bytes Throughput Over Time and save it with %REPORTDIR%\charts\BytesThroughputOverTime.png name;
%JAVA_HOME%\bin\java.exe -jar %JMETER_HOME%\lib\ext\CMDRunner.jar --tool Reporter --generate-png %REPORTDIR%\charts\BytesThroughputOverTime.png --input-jtl %INPUT_JTL_FILE% --plugin-type BytesThroughputOverTime --width %CHART_WIDTH% --height %CHART_HEIGHT% --relative-times no --granulation %CHART_GRANULATION_IN_MS% --auto-scale yes --paint-gradient no
%JAVA_HOME%\bin\java.exe -jar %JMETER_HOME%\lib\ext\CMDRunner.jar --tool Reporter --generate-csv %REPORTDIR%\data\BytesThroughputOverTime.csv --input-jtl %INPUT_JTL_FILE% --plugin-type BytesThroughputOverTime --relative-times no --granulation %CHART_GRANULATION_IN_MS%
%JAVA_HOME%\bin\java.exe -cp .\lib\saxon9he.jar net.sf.saxon.Transform -o:%REPORTDIR%\data\BytesThroughputOverTime.xml -it:main .\_template\xsl\csv-to-xml_v2.xslt pathToCSV="file:%REPORTDIR_xml%/data/BytesThroughputOverTime.csv" 
%JAVA_HOME%\bin\java.exe org.apache.xalan.xslt.Process -IN %REPORTDIR%\data\BytesThroughputOverTime.xml -XSL %INPUT_TEMPLATE_PATH%\xsl\graphs.xsl -OUT %REPORTDIR%\details\report_bthot.html -param dateReport "%DATE_TIME_FMT%" -param graphName "BytesThroughputOverTime" -param graphChart "../charts/BytesThroughputOverTime.png"

rem # build chart for Overall Response Times for all measuremets and save it with %REPORTDIR%\charts\ResponseTimesOverTime.png name;
%JAVA_HOME%\bin\java.exe -jar %JMETER_HOME%\lib\ext\CMDRunner.jar --tool Reporter --generate-png %REPORTDIR%\charts\AllResponseTimeOverTime.png --input-jtl %INPUT_JTL_FILE% --plugin-type ResponseTimesOverTime --width %CHART_WIDTH% --height %CHART_HEIGHT% --relative-times no --granulation %CHART_GRANULATION_IN_MS% --aggregate-rows yes --auto-scale yes --paint-gradient no
%JAVA_HOME%\bin\java.exe -jar %JMETER_HOME%\lib\ext\CMDRunner.jar --tool Reporter --generate-csv %REPORTDIR%\data\AllResponseTimeOverTime.csv --input-jtl %INPUT_JTL_FILE% --plugin-type ResponseTimesOverTime --relative-times no --granulation %CHART_GRANULATION_IN_MS% --aggregate-rows yes
%JAVA_HOME%\bin\java.exe -cp .\lib\saxon9he.jar net.sf.saxon.Transform -o:%REPORTDIR%\data\AllResponseTimeOverTime.xml -it:main .\_template\xsl\csv-to-xml_v2.xslt pathToCSV="file:%REPORTDIR_xml%/data/AllResponseTimeOverTime.csv" 
%JAVA_HOME%\bin\java.exe org.apache.xalan.xslt.Process -IN %REPORTDIR%\data\AllResponseTimeOverTime.xml -XSL %INPUT_TEMPLATE_PATH%\xsl\graphs.xsl -OUT %REPORTDIR%\details\report_artot.html -param dateReport "%DATE_TIME_FMT%" -param graphName "OverallResponseTimes" -param graphChart "../charts/AllResponseTimeOverTime.png" 

rem # build chart for Times Vs Threads for all measuremets and save it with %REPORTDIR%\charts\TimesVsThreads.png name;
rem # chart with %REPORTDIR%\charts\TimesVsThreads.png name is hardcoded in the %REPORTDIR%/details/report_charts_jm.html;
%JAVA_HOME%\bin\java.exe -jar %JMETER_HOME%\lib\ext\CMDRunner.jar --tool Reporter --generate-png %REPORTDIR%\charts\TimesVsThreads.png --input-jtl %INPUT_JTL_FILE% --plugin-type TimesVsThreads --width %CHART_WIDTH% --height %CHART_HEIGHT% --granulation %CHART_GRANULATION_IN_MS%
%JAVA_HOME%\bin\java.exe -jar %JMETER_HOME%\lib\ext\CMDRunner.jar --tool Reporter --generate-csv %REPORTDIR%\data\TimesVsThreads.csv --input-jtl %INPUT_JTL_FILE% --plugin-type TimesVsThreads --granulation %CHART_GRANULATION_IN_MS%
%JAVA_HOME%\bin\java.exe -cp .\lib\saxon9he.jar net.sf.saxon.Transform -o:%REPORTDIR%\data\TimesVsThreads.xml -it:main .\_template\xsl\csv-to-xml_v2.xslt pathToCSV="file:%REPORTDIR_xml%/data/TimesVsThreads.csv" 
%JAVA_HOME%\bin\java.exe org.apache.xalan.xslt.Process -IN %REPORTDIR%\data\TimesVsThreads.xml -XSL %INPUT_TEMPLATE_PATH%\xsl\graphs.xsl -OUT %REPORTDIR%\details\report_TimesVsThreads.html -param graphName "Response Times VS Threads" -param graphChart "../charts/TimesVsThreads.png"
rem # build chart for Response Times Over Time for all measuremets and save it with %REPORTDIR%\charts\ResponseTimesOverTime.png name;
%JAVA_HOME%\bin\java.exe -jar %JMETER_HOME%\lib\ext\CMDRunner.jar --tool Reporter --generate-png %REPORTDIR%\charts\ResponseTimesOverTime.png --input-jtl %INPUT_JTL_FILE% --plugin-type ResponseTimesOverTime --width %CHART_WIDTH% --height %CHART_HEIGHT% --relative-times no --granulation %CHART_GRANULATION_IN_MS% --auto-scale yes --paint-gradient no
%JAVA_HOME%\bin\java.exe -jar %JMETER_HOME%\lib\ext\CMDRunner.jar --tool Reporter --generate-csv %REPORTDIR%\data\ResponseTimesOverTime.csv --input-jtl %INPUT_JTL_FILE% --plugin-type ResponseTimesOverTime --relative-times no --granulation %CHART_GRANULATION_IN_MS%
%JAVA_HOME%\bin\java.exe -cp .\lib\saxon9he.jar net.sf.saxon.Transform -o:%REPORTDIR%\data\ResponseTimesOverTime.xml -it:main .\_template\xsl\csv-to-xml_v2.xslt pathToCSV="file:%REPORTDIR_xml%/data/ResponseTimesOverTime.csv" 
%JAVA_HOME%\bin\java.exe org.apache.xalan.xslt.Process -IN %REPORTDIR%\data\ResponseTimesOverTime.xml -XSL %INPUT_TEMPLATE_PATH%\xsl\graphs.xsl -OUT %REPORTDIR%\details\report_rtot.html -param dateReport "%DATE_TIME_FMT%" -param graphName "ResponseTimesOverTime" -param graphChart "../charts/ResponseTimesOverTime.png" 

rem # build chart for Response Codes Per Second and save it with %REPORTDIR%\charts\ResponseCodesPerSecond.png name;
%JAVA_HOME%\bin\java.exe -jar %JMETER_HOME%\lib\ext\CMDRunner.jar --tool Reporter --generate-png %REPORTDIR%\charts\ResponseCodesPerSecond.png --input-jtl %INPUT_JTL_FILE% --plugin-type ResponseCodesPerSecond --width %CHART_WIDTH% --height %CHART_HEIGHT% --relative-times no --granulation %CHART_GRANULATION_IN_MS% --auto-scale yes --paint-gradient no
%JAVA_HOME%\bin\java.exe -jar %JMETER_HOME%\lib\ext\CMDRunner.jar --tool Reporter --generate-csv %REPORTDIR%\data\ResponseCodesPerSecond.csv --input-jtl %INPUT_JTL_FILE% --plugin-type ResponseCodesPerSecond --relative-times no --granulation %CHART_GRANULATION_IN_MS%
%JAVA_HOME%\bin\java.exe -cp .\lib\saxon9he.jar net.sf.saxon.Transform -o:%REPORTDIR%\data\ResponseCodesPerSecond.xml -it:main .\_template\xsl\csv-to-xml_v2.xslt pathToCSV="file:%REPORTDIR_xml%/data/ResponseCodesPerSecond.csv" 
%JAVA_HOME%\bin\java.exe org.apache.xalan.xslt.Process -IN %REPORTDIR%\data\ResponseCodesPerSecond.xml -XSL %INPUT_TEMPLATE_PATH%\xsl\graphs.xsl -OUT %REPORTDIR%\details\report_rcps.html -param dateReport "%DATE_TIME_FMT%" -param graphName "ResponseCodesPerSecond" -param graphChart "../charts/ResponseCodesPerSecond.png"

rem # build chart for Transactions Per Second for all measuremets and save it with %REPORTDIR%\charts\TransactionsPerSecond.png name;
%JAVA_HOME%\bin\java.exe -jar %JMETER_HOME%\lib\ext\CMDRunner.jar --tool Reporter --generate-png %REPORTDIR%\charts\TransactionsPerSecond.png --input-jtl %INPUT_JTL_FILE% --plugin-type TransactionsPerSecond --width %CHART_WIDTH% --height %CHART_HEIGHT% --relative-times no --granulation %CHART_GRANULATION_IN_MS% --auto-scale yes --paint-gradient no
%JAVA_HOME%\bin\java.exe -jar %JMETER_HOME%\lib\ext\CMDRunner.jar --tool Reporter --generate-csv %REPORTDIR%\data\TransactionsPerSecond.csv --input-jtl %INPUT_JTL_FILE% --plugin-type TransactionsPerSecond --relative-times no --granulation %CHART_GRANULATION_IN_MS%
%JAVA_HOME%\bin\java.exe -cp .\lib\saxon9he.jar net.sf.saxon.Transform -o:%REPORTDIR%\data\TransactionsPerSecond.xml -it:main .\_template\xsl\csv-to-xml_v2.xslt pathToCSV="file:%REPORTDIR_xml%/data/TransactionsPerSecond.csv" 
%JAVA_HOME%\bin\java.exe org.apache.xalan.xslt.Process -IN %REPORTDIR%\data\TransactionsPerSecond.xml -XSL %INPUT_TEMPLATE_PATH%\xsl\graphs.xsl -OUT %REPORTDIR%\details\report_tps.html -param dateReport "%DATE_TIME_FMT%" -param graphName "TransactionsPerSecond" -param graphChart "../charts/TransactionsPerSecond.png"

rem # build chart for Latencies Over Time for all measuremets and save it with %REPORTDIR%\charts\LatenciesOverTime.png name;
rem %JAVA_HOME%\bin\java.exe -jar %JMETER_HOME%\lib\ext\CMDRunner.jar --tool Reporter --generate-png %REPORTDIR%\charts\LatenciesOverTime.png --input-jtl %INPUT_JTL_FILE% --plugin-type LatenciesOverTime --width %CHART_WIDTH% --height %CHART_HEIGHT% --relative-times no --granulation %CHART_GRANULATION_IN_MS% --auto-scale yes --paint-gradient no
rem %JAVA_HOME%\bin\java.exe -cp .\lib\saxon9he.jar net.sf.saxon.Transform -o:%REPORTDIR%\data\LatenciesOverTime.xml -it:main .\_template\xsl\csv-to-xml_v2.xslt pathToCSV="file:%REPORTDIR_xml%/data/LatenciesOverTime.csv" 
rem %JAVHOME%\bin\java.exe org.apache.xalan.xslt.Process -IN %REPORTDIR%\data\LatenciesOverTime.xml -XSL %INPUT_TEMPLATE_PATH%\xsl\graphs.xsl -OUT %REPORTDIR%\details\report_lot.html -param dateReport "%DATE_TIME_FMT%" -param graphName "LatenciesOverTime" -param graphChart "../charts/LatenciesOverTime.png"

copy %JMX_HOME%\Listeners\%ENV%.%PRODUCT%\*.html %REPORTDIR%\details

rem delete temp files from /data
del %REPORTDIR%\data\*.* /s /f /q

rem copy archives of test data and errors log to the report folder
REM %JAVA_HOME%\bin\jar.exe cf %REPORTDIR%\data\errors.jmeter.zip %ERRORS_FILE%
"c:\Program Files\Java\jdk1.8.0_111\bin\jar.exe" cf %REPORTDIR%\data\input.jmeter.zip %INPUT_JTL_FILE%
"c:\Program Files\Java\jdk1.8.0_111\bin\jar.exe" cf %REPORTDIR%\data\log.jmeter.zip %JMX_HOME%\jmeter.log

rem # the code to generate csv files for response times
REM %JAVA_HOME%\bin\java.exe -jar %JMETER_HOME%\lib\ext\CMDRunner.jar --tool Reporter --generate-csv %REPORTDIR%\data\AggregateReport.csv --input-jtl %INPUT_JTL_FILE% --plugin-type AggregateReport
REM %JAVA_HOME%\bin\java.exe -jar %JMETER_HOME%\lib\ext\CMDRunner.jar --tool Reporter --generate-csv %REPORTDIR%\data\SynthesisReport.csv --input-jtl %INPUT_JTL_FILE% --plugin-type SynthesisReport

rem # prepare info about errors
%JAVA_HOME%\bin\java.exe -Xmx1024M org.apache.xalan.xslt.Process -IN %INPUT_JTL_FILE% -XSL %INPUT_TEMPLATE_PATH%\xsl\errors-detailed.xsl -OUT %REPORTDIR%\details\errors_detailed.html -param dateReport "%DATE_TIME_FMT%"

rem # prepare detailed report
rem %JAVA_HOME%\bin\java.exe -Xmx1024M org.apache.xalan.xslt.Process -IN %INPUT_JTL_FILE% -XSL %INPUT_TEMPLATE_PATH%\xsl\transactions-detailed.xsl -OUT %REPORTDIR%\details\transactions_detailed.html -param dateReport "%DATE_TIME_FMT%"

rem # prepare report.html
%JAVA_HOME%\bin\java.exe -Xmx4G org.apache.xalan.xslt.Process -IN %INPUT_JTL_FILE% -XSL %INPUT_TEMPLATE_PATH%\xsl\jmeter-results-report_full.xsl -OUT %REPORTDIR%\report.html -param dateReport "%DATE_TIME_FMT%" -param ScriptName %SCRIPT_FILE% -param Environment %ENV%.%PRODUCT% -param LogoName logo_%APPLICATION%.png

rem goto :exec_batch
