rem set JAVA_HOME="c:\Program Files\Java\jre1.8.0_144"
set JAVA_HOME="c:\Program Files\Java\jre1.8.0_151"
set JMETER_HOME=d:\jMeter-3.0
set JMX_HOME=d:\PerfTest

rem :exec_batch

rem ENV=test, dev, prod(www), test.beta
set ENV=%~1
set APPLICATION=%~2
set DOMAIN=%~3
set PRODUCT=%APPLICATION%.%DOMAIN%

set JENKINS_BUILD_NUMBER=%4
set SCRIPT_NAME=%~5
set SCRIPT_FILE=%JMX_HOME%\Scripts\%SCRIPT_NAME%

set INPUT_JTL_FILE=%JMX_HOME%\Listeners\%ENV%.%PRODUCT%\Publisher_results.xml

set CLASSPATH=%CLASSPATH%;%JMETER_HOME%\lib\xalan-2.7.2.jar;%JMETER_HOME%\lib\serializer-2.7.2.jar;%JMETER_HOME%\lib\xercesImpl-2.11.0.jar;%JMETER_HOME%\lib\xml-apis-1.4.01.jar;.\lib\jcommon-1.0.23.jar;.\lib\jfreechart-1.0.19.jar;.\lib\reporter.jar

set INPUT_TEMPLATE_PATH=.\_template
rem variables for width and height of charts in pixels.
set CHART_WIDTH=1200
set CHART_HEIGHT=600
rem variable for granulation in ms
set CHART_GRANULATION_IN_MS=30000

rem /*generate timestamp for new report folder*/
for /f %%a IN ('WMIC OS GET LocalDateTime ^| find "."') do set DTS=%%a
set DATE_TIME=%DTS:~0,8%
set DATE_TIME_FMT=%DTS:~0,4%.%DTS:~4,2%.%DTS:~6,2% %DTS:~8,2%:%DTS:~10,2%
rem echo '%DATE_TIME_FMT%'
rem /*create folders and subfolders for the new report and copy template files from appropriate _template folder*/
set REPORTDIR=.\reports\%DATE_TIME%_%JENKINS_BUILD_NUMBER%_%ENV%.%PRODUCT%
set REPORTDIR_xml=./reports/%DATE_TIME%_%JENKINS_BUILD_NUMBER%_%ENV%.%PRODUCT%

mkdir %REPORTDIR%
mkdir %REPORTDIR%\static
mkdir %REPORTDIR%\static\images
mkdir %REPORTDIR%\static\css
mkdir %REPORTDIR%\static\js
mkdir %REPORTDIR%\details
mkdir %REPORTDIR%\charts
mkdir %REPORTDIR%\data

copy %INPUT_TEMPLATE_PATH%\static\images\* %REPORTDIR%\static\images
copy %INPUT_TEMPLATE_PATH%\static\css\* %REPORTDIR%\static\css
copy %INPUT_TEMPLATE_PATH%\static\js\* %REPORTDIR%\static\js

copy %JMX_HOME%\Listeners\%ENV%.%PRODUCT%\*.html %REPORTDIR%\details

rem # build chart for Threads State Over Time and save it with %REPORTDIR%\charts\ThreadsStateOverTime.png name;
rem # generate-png
%JAVA_HOME%\bin\java.exe -jar %JMETER_HOME%\lib\ext\CMDRunner.jar --tool Reporter --generate-png %REPORTDIR%\charts\ThreadsStateOverTime.png --input-jtl %INPUT_JTL_FILE% --plugin-type ThreadsStateOverTime --width %CHART_WIDTH% --height %CHART_HEIGHT% --relative-times no --granulation %CHART_GRANULATION_IN_MS% --paint-gradient no
rem # generate-csv
%JAVA_HOME%\bin\java.exe -jar %JMETER_HOME%\lib\ext\CMDRunner.jar --tool Reporter --generate-csv %REPORTDIR%\data\ThreadsStateOverTime.csv --input-jtl %INPUT_JTL_FILE% --plugin-type ThreadsStateOverTime --relative-times no --granulation %CHART_GRANULATION_IN_MS%
rem # transfer csv to xml
%JAVA_HOME%\bin\java.exe -cp .\lib\saxon9he.jar net.sf.saxon.Transform -o:%REPORTDIR%\data\ThreadsStateOverTime.xml -it:main .\_template\xsl\csv-to-xml_v2.xslt pathToCSV="file:%REPORTDIR_xml%/data/ThreadsStateOverTime.csv" 
rem # transfer xml to html
%JAVA_HOME%\bin\java.exe org.apache.xalan.xslt.Process -IN %REPORTDIR%\data\ThreadsStateOverTime.xml -XSL %INPUT_TEMPLATE_PATH%\xsl\graphs.xsl -OUT %REPORTDIR%\details\report_thot.html -param dateReport "%DATE_TIME_FMT%" -param graphName "ThreadsStateOverTime" -param graphChart "../charts/ThreadsStateOverTime.png"
 
rem delete temp files from /data
REM del %REPORTDIR%\data\*.* /s /f /q

rem # prepare report.html
%JAVA_HOME%\bin\java.exe -Xmx4G org.apache.xalan.xslt.Process -IN %INPUT_JTL_FILE% -XSL %INPUT_TEMPLATE_PATH%\xsl\jmeter-results-report_full.xsl -OUT %REPORTDIR%\report.html -param dateReport "%DATE_TIME_FMT%" -param ScriptName %SCRIPT_FILE% -param Environment %ENV%.%PRODUCT% -param LogoName logo_%APPLICATION%.png

rem goto :exec_batch
